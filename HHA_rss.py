import requests, json, datetime, hashlib
from typing import Type
from feedgen.feed import FeedGenerator
from bs4 import BeautifulSoup

baseurl = 'https://cluster.schneller-durch-hamburg.de/wp-content/themes/sdh-cms/iframe/data/hb-maps-detail-{0}.json'

pages = [
    'u4-01-hr',
    'u4-02-ss',
    'u4-03-hg',
    'u5-01-cn',
    'u5-02-ss',
    'u5-03-bn',
    'u5-04-sp',
    'u5-05-bf',
]

sha1 = hashlib.sha1(usedforsecurity=False)

class Location ():
    def __init__(self, data, URL):
        self.name = data['TITLE']
        self.code = data['HEADER_DETAIL_CLASS']
        self.line = self.code[0:2].upper()
        self.link = URL

class Announcement():
    def __init__(self,parent: Type[Location],data):
        self.parent = parent
        self.code = data['POPUP_ID'][4:-7]

class Content():
    def __init__(self, parent: Type[Announcement],data):
        self.parent = parent
        self.code = data['CONTENT_ELEMENT_ID'][4:-11]
        self.type = data['CONTENT_FLAG_CLASS']
        self.period = data['CONTENT_PERIOD']
        self.title = data['CONTENT_TITLE']
        self.content = data['CONTENT_ELEMENTS'][0]['data']['CONTENT_ELEMENT']

contentlist = []


for page in pages:
    URL = baseurl.format(page)
    response = requests.get(URL)
    data = json.loads(response.text)
    print(data['TITLE'])

    loc = Location(data, URL)
    for popup in data['_POPUPS']:
        data = popup['data']
        ann = Announcement(loc, data)
        for content in data['_CONTENT']:
            data = content['data']
            con = Content(ann, data)
            contentlist.append(con)

print('done')


fg = FeedGenerator()
# fg.id('http://lernfunk.de/media/654321')
fg.title('Maßnahmenkatalog schneller-durch-hamburg.de')
fg.author( {'name':'HOCHBAHN','email':'hochbahn.de'} )
fg.link( href='http://hochbahn.de', rel='alternate' )
# fg.logo('http://ex.com/logo.jpg')
# fg.subtitle('This is a cool feed!')
# fg.link( href='http://larskiesow.de/test.atom', rel='self' )
fg.language('de')
fg.description('Automatically generated')


try:
    ts = json.loads(requests.get('https://rss.echtnurich.de/hha_timestamps.json').text)
except Exception as e:
    print('load failed:',e)
    ts = {}

for content in contentlist:
    contenttext = BeautifulSoup(content.content).get_text()
    announcement = content.parent
    location = announcement.parent
    sha1.update(contenttext.encode('utf8'))
    hashid = str(sha1.hexdigest())
    pubdate = datetime.datetime.now()

    if hashid not in ts.keys():
        #print('New content: ' + location.line + ': ' + content.title + ' - ' + location.name)
        print(hashid + 'not in ' + str(ts.keys())[0:-1])
    

    ts.setdefault(hashid,pubdate.strftime('%a, %d %b %Y, %H:%M'))
    pubdate = datetime.datetime.strptime(ts[hashid],'%a, %d %b %Y, %H:%M')

    description = content.period
    if content.type == 'content-aktuell':
        description = 'Neu: '+description

    fe = fg.add_entry()
    fe.id(location.code+'-'+content.code+'#'+hashid)
    fe.title(location.line + ': ' + content.title + ' - ' + location.name)
    fe.link(href=location.link)
    fe.content(contenttext)
    fe.published(pubdate.replace(tzinfo=datetime.timezone.utc))

fg.rss_str(pretty=True)
fg.rss_file('hha.xml')

# BANISH OLD WRONG UNSTABLE HASHES
newts = {}
for key,value in ts.items():
    if datetime.datetime.strptime(value,'%a, %d %b %Y, %H:%M') < datetime.datetime(2024, 3, 13, 11, 50, 0, 0):
        print('removed ' + value)
    else:
        newts[key] = value

with open('hha_timestamps.json', 'w') as ts_file:
    json.dump(newts, ts_file, indent=4)

